package com.example.ticTayToe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
public class TicTacToeTest {
    @InjectMocks
    private TicApp app;

    @Test
    void whenXIsOutsideArea(){
     assertThatThrownBy(()->app.play(4,0))
             .isInstanceOf(RuntimeException.class)
             .hasMessage("X is outside coordinate");
    }

    @Test
    void whenOIsOutsideArea(){
        assertThatThrownBy(()->app.play(0,4))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("O is outside coordinate");
    }

    @Test
    void whenBoardIsOccupied(){
        app.play(0,0);
        assertThatThrownBy(()->app.play(0,0))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Board is occupied");
    }
    @Test
    void fistPlayerX(){
        assertThat(app.nextPlayer()).isEqualTo('X');

    }
    @Test
    void nextPlayerX(){
        app.play(0,0);
        assertThat(app.nextPlayer()).isEqualTo('O');

    }

    @Test
    void noWinnerOrContinue(){
        app.play(0,0);
        app.play(0,2);
        app.play(0,1);
        app.play(1,0);
        app.play(1,2);
        app.play(1,1);
        app.play(2,0);
        app.play(2,2);
        //app.play(2,1);
        assertThat(app.play(2,1)).isEqualTo("Draw");
    }

    @Test
    void WinnerIsHorizontal(){
        app.play(0,0);
        app.play(1,1);
        app.play(0,1);
        app.play(2,2);


        assertThat(app.play(0,2)).isEqualTo("X is the winner");
    }
    @Test
    void WinnerIsVertical(){
        app.play(0,0);
        app.play(0,1);
        app.play(0,2);
        app.play(1,1);
        app.play(2,2);


        assertThat(app.play(2,1)).isEqualTo("O is the winner");
    }
    @Test
    void WinnerIsDiagonal(){
        app.play(0,0);
        app.play(0,1);
        app.play(1,1);
        app.play(2,1);



        assertThat(app.play(2,2)).isEqualTo("X is the winner");
    }
   @Test
    void endGameThenRestart (){
       app.play(0,0);
       app.play(0,1);
       app.play(1,1);
       app.play(2,1);
       assertThat(app.play(2,2)).isEqualTo("X is the winner");
       assertThat(app.nextPlayer()).isEqualTo('X');
       assertThat(app.play(2,2)).isEqualTo("No winner");
   }


}
