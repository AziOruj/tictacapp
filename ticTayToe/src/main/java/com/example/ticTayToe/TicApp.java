package com.example.ticTayToe;

import java.sql.Array;
import java.sql.SQLOutput;
import java.util.Arrays;

public class TicApp {
    String resultText;
    private int move=1;
    private char lastPlayer=' ';
    private char [][] board= {{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};

    public String  play(int xCord,int oCord){
        lastPlayer=nextPlayer();
        checkCord(xCord,'X');
        checkCord(oCord,'O');
        checkBoardOccupied(xCord,oCord);
        printBoard();
        if (move++ >=5 && testEndGame()){
            restart();
            return resultText; }
        return "No winner";
    }

    private boolean isWinVertical(){
        for (int a=0;a<3;a++){
            if (board[0][a]==lastPlayer&&
                    board[1][a]==lastPlayer&&
                    board[2][a]==lastPlayer){
                return true; }
        }
        return false;
    }
    private boolean isWinHorizontal(){
        for (int a=0;a<3;a++){
            if (board[a][0]==lastPlayer&&
                    board[a][1]==lastPlayer&&
                    board[a][2]==lastPlayer){
                return true;
            }
        }
        return false;
    }
    public void checkCord(int x,char a){
        if (x>2){
            throw new RuntimeException(a+" is outside coordinate");
        }
    }
    public void checkBoardOccupied(int x,int  y){
        if (board[x][y] != ' '){
            throw new RuntimeException("Board is occupied");
        }else{
            board[x][y]=lastPlayer;
        }
    }
    public char nextPlayer(){
        if (lastPlayer=='X'){
            return 'O';
        }
        return 'X';
    }

    public boolean isWinIsDiagonal(){
        if (board[1][1] == lastPlayer){
            for (int a=0;a<3;a+=2){
                if (board[0][a]==lastPlayer &&
                        board[2][2-a]==lastPlayer){
                    return true;
                }
        }
        }
        return false;
    }
    public void restart(){
        if (testEndGame()){
            board= new char[][]{{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};
            move=1;
            lastPlayer=' '; }
    }
    public boolean testEndGame(){
        if (isWinHorizontal()|| isWinVertical()||isWinIsDiagonal()){
            resultText=  lastPlayer+" is the winner";
            return true; }
        else if (move==10){
            resultText="Draw";
            return true; }
         return false;

    }
    public void printBoard(){
        System.out.println("====Board status ====");
        System.out.println(Arrays.deepToString(board));
    }
}
