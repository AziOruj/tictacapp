package com.example.ticTayToe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicTayToeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicTayToeApplication.class, args);
	}

}
